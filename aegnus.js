/* 
   ÆGNUS:  A GNU social Client  <http://aegn.us/>
   WebExtension version.

   This is free software. It is distributed under the 
   GNU General Public License. <https://www.gnu.org/copyleft/gpl.html>

   -----
   Latest fixes:
   - !group and @user mentions were case-sensitive, which didn't work.
     Now they're not. 
   - In the CSS file, 'cursor' on post headers is now 'context-menu' so
     there's actually a cue that there are options under there!
   -----
*/
// *** THIS THING IS A MESS ***
// =====================================================================
// F U N C T I O N S . . .

function getNodeUrl(){
    var nodeURL = (localStorage.GNUsocialNodeSSL ? "https://" : "http://") +
	localStorage.GNUsocialNode;
    return nodeURL;
}

function loadSettings(){
    var userNickInput = document.getElementById('userNick');
    var userNodeInput = document.getElementById('userNode');
    var userNodeSSLToggle = document.getElementById('serverSSL');
    var formCKey = document.getElementById('OAuthCKey');
    var formCSecret = document.getElementById('OAuthCSecret');
    var formProxy = document.getElementById('customProxy');
    var formRefresh = document.getElementById('refreshRate');
    
    userNickInput.value = localStorage.GNUsocialUser;
    userNodeInput.value = localStorage.GNUsocialNode;
    userNodeSSLToggle.checked = localStorage.GNUsocialNodeSSL;
    formCKey.value = localStorage.aegnusCKey;
    formCSecret.value = localStorage.aegnusCSecret;
    formProxy.value = localStorage.aegnusProxy;
    formRefresh.value = localStorage.aegnusRefresh;
}

function applySettings(){
    // Store user settings from the Settings panel.
    
    var userNickInput = document.getElementById('userNick');
    var userNodeInput = document.getElementById('userNode');
    var userNodeSSLToggle = document.getElementById('serverSSL');
    var formCKey = document.getElementById('OAuthCKey');
    var formCSecret = document.getElementById('OAuthCSecret');
    var formProxy = document.getElementById('customProxy');
    var formRefresh = document.getElementById('refreshRate');
    
    localStorage.GNUsocialUser = userNickInput.value.trim();
    localStorage.GNUsocialNode = userNodeInput.value.trim();
    localStorage.GNUsocialNodeSSL = userNodeSSLToggle.checked;
    localStorage.aegnusCKey = formCKey.value.trim();
    localStorage.aegnusCSecret = formCSecret.value.trim();
    localStorage.aegnusProxy = formProxy.value.trim();
    localStorage.aegnusRefresh = parseInt(formRefresh.value);
}

function getFeedPath(f){
    var feedPaths = { "public"  : "/api/statuses/public_timeline.atom",
		      "network" : "/api/statuses/networkpublic_timeline.atom",
		      "home"    : "/api/statuses/friends_timeline/" +
		      localStorage.GNUsocialUser + ".atom",
		      "outbox"  : "/api/statuses/user_timeline/" +
		      localStorage.GNUsocialUser + ".atom",
		      "replies" : "/api/statuses/mentions/" + localStorage.GNUsocialUser + ".atom",
		      //"favorites" : "/api/statusnet/app/favorites/" + localUserID + ".atom",
		      "favorites" : "/api/favorites/" + localStorage.GNUsocialUser + ".atom",
		      "search" : "/api/search.atom"}
    return feedPaths[f];
}
function permalinkToAtomUrl(url){
    var splitUrl = url.split("/");
    return splitUrl[0] + "//" + splitUrl[2] + "/api/statuses/show/" + splitUrl[splitUrl.length-1] + ".atom";
}

function getActivity(url,jump){
    // Fetch a single activity <entry> and merge it into the timeline.
    // Pass me an Atom API url (ex. https://server/api/statuses/show/12345.atom)
    var xhr = new XMLHttpRequest();
    xhr.onload = function(url,jump){
	if(this.status != 200){
	    barf("Failed to fetch " + url + " -- HTTP " + this.status);
	    return;
	}
	var tempFeed = document.implementation.createDocument("http://www.w3.org/2005/Atom","feed");
	tempFeed.characterSet = "utf-8";
	var selfLink = tempFeed.createElement("link");
	selfLink.setAttribute("rel","self");
	selfLink.setAttribute("href",url);
	tempFeed.querySelector("feed").appendChild(selfLink);
	tempFeed.querySelector("feed").appendChild(this.responseXML.querySelector("entry"));
	tlItems = mergeTl(objectify(tempFeed),tlItems);
	if (postEditor.style.display != "block"){
	    renderTimeline();
	}
	if (jump) window.location.hash = "#" + tempFeed.querySelector("id").textContent;
    }.bind(xhr,url,jump);
    xhr.open("GET",proxyString + url,true);
    xhr.send();
}

function getTimeline(t){
    // Fetch timeline entries from the server, and merge them with the
    // existing timeline if necessary. The passed value, 't', may be
    // an http(s) URL, or a keyword string. If no value is passed, we
    // simply refresh the current timeline.

    if (!t) {
	t = currentTimeline;
    } else {
	// Erase timeline <div> for the new timeline.
	document.body.classList.add("colorcycle");
	timelineMain.innerHTML = "";
    }

    console.log("Fetching " + t);

    // A timer might be set to refresh the timeline. Clear that
    // timeout so that when a user switches timelines we don't end-up
    // having a couple of timelines loading at the same time

    clearTimeout(timelineRefresh);
    
    var gsUser = localStorage.GNUsocialUser;
    var gsServer = getNodeUrl();
    var signed = true;
    
    if(t.startsWith("http")){
	var feedURL = t;
	signed = false;
    } else {
	var feedURL = gsServer + getFeedPath(t);
    }
    
    request = new XMLHttpRequest();
    request.onload = function(t) {
	document.body.classList.remove("colorcycle");
	if (this.status != 200){
	    alert("feed unavailable:\n\nHTTP" + this.status + "\n" + this.responseURL);
	} else {
	    // Did we get the home timeline? Take this opportunity to
	    // update the signed-in avatar image.
	    // *** TODO: Move this into userInit() and get the avatar from our FOAF ***
	    if(t == "home"){
		var avatarImg = document.getElementById("userAvatar");
		avatarImg.src = this.responseXML.querySelector("feed > logo").textContent;
	    }

	    timelineTitle.textContent = this.responseXML.querySelector("feed > title").textContent;

	    var newTL = objectify(this.responseXML);
	    if (t == currentTimeline) { 
		tlItems = mergeTl(newTL,tlItems);
	    } else {
		tlItems = newTL;
	    }

	    getSources();
	    
	    // Reset the current-page counter if we've switched to a new timeline
	    if (t != currentTimeline){
		currentTimelinePage = 1;
		currentTimeline = t;
	    }

	    // Don't redraw the timeline if we're in the middle of editing
	    // a post.
	    if (postEditor.style.display != "block"){
		renderTimeline();
	    }
	    
	    timelineRefresh = setTimeout(getTimeline,300000);
	}
    }.bind(request,t);
    if(signed){
	request.open('GET',feedURL + "?" + signRequest("GET",feedURL),true);
    } else {
	request.open('GET',feedURL,true);
    }
    request.send();
}

function getNextPage(){
    // Fetch timeline items from the server, and merge them with the
    // existing timeline if necessary.

    // *** TODO: This should really be merged with getTimeline, since
    // *** they're ~80% identical.
    
    var gsUser = localStorage.GNUsocialUser;
    var gsServer = getNodeUrl();

    var signed = true;
    if(currentTimeline.startsWith("http")){
	signed = false;
	var feedURL = gsServer + currentTimeline;
    } else {
        var feedURL = gsServer + getFeedPath(currentTimeline);
    }
    
    currentTimelinePage++;

    // Stop additional pages from being requested until this is cleared.
    fetching = true;
    
    request = new XMLHttpRequest();
    request.onload = function() {
	// Clear the blocking flag.
	fetching = false;
	nextPage.innerHTML=`Click here to load more activities.`;
	
	if (this.status != 200){
	    alert("feed unavailable:\n\nHTTP " + this.status + "\n" + this.responseURL);
	} else {
	    var newTL = objectify(this.responseXML);
	    tlItems = mergeTl(newTL,tlItems);
	    getSources();
	    renderTimeline(tlItems);
	}
    }.bind(request);
    
    if(signed){
	request.open('GET',
		     feedURL + "?" +
		     signRequest("GET",feedURL,"","&page="+currentTimelinePage),
		     true);
    } else {
	request.open('GET',feedURL + "?" + "page=" + currentTimelinePage,true);
    }    
    request.send();
}

function mergeTl(newTl,tl){    
    // Iterate over the incoming timeline items, and add them to the
    // timeline items object if they're new.
    var newItem;
    var oldItem;
    for (var ix = 0; ix < newTl.length ; ix++) {
	newItem = newTl[ix];
	oldItem = hasDuplicate(newItem, tl,"id");
	if (oldItem === false){
	    tl.push(newItem);
	} else {
	}
    }
    return tl;
}

function getSources(){
    // Look through the timeline for favorites and shares; if their activity object
    // is not in the timeline by itself, go get it.
    var fetchesInProgress = {};
    for (var ix = 0; ix < tlItems.length; ix++){
	//if(!(tlItems[ix].activityVerb == "share" || 
	//     tlItems[ix].activityVerb == "favorite")) continue;
	// Maybe don't fetch likes? Yeah. Don't fetch likes.
	if(tlItems[ix].activityVerb != "share") continue; 
	if(!fetchesInProgress[tlItems[ix].object.id]){
	    if(tlItems[ix].object.localId){
		var sourceUrl = getNodeUrl() + "/api/statuses/show/" +
		    tlItems[ix].object.localId + ".atom";
	    } else {
		var plink = tlItems[ix].object.url.split('/');
		var sourceUrl = plink[0] + "//" + plink[2] +
		    "/api/statuses/show/" + plink[plink.length-1] + ".atom";
	    }
	    fetchesInProgress[tlItems[ix].object.id] = true;
	    getActivity(sourceUrl);
	}
    }
}


function renderTimeline(){
    // This is the part where we actually put the items in the document.

    tlItems.sort(timeCompare); // Defined in convo.js

    // Create (or reorder preexisting) HTML elements in the timeline <div>
    // Notice that we're doing this in reverse order. Timelines are most-recent-on-top,
    // conversations are most-recent-at-bottom.
    for (var i = tlItems.length-1; i >= 0; i--){
	var e;
	if (document.getElementById(tlItems[i].id)){
	    e = document.getElementById(tlItems[i].id);
	} else {
	    e = itemToTimelineElement(tlItems[i]);
	}
	// Make sure we're not stealing items from the conversation view
	if(!convoArea.querySelector('article[id="' + tlItems[i].id + '"]')){
	    timelineMain.appendChild(e);
	}
    }

    // Look for favorites and shares, and stick them inside the drawer
    // of any items that they belong to.

    // Okay, I want to do this differently, here. I've already changed
    // the stylesheet so that likes outside of an entry's drawer are
    // hidden. I want likes to be non-bumping. Shares, on the other
    // hand, I want to "bump" items up near the top of the timeline.

    for (var i = tlItems.length-1; i >= 0; i--){
	var item = tlItems[i];
	if (item.activityVerb == "favorite" || item.activityVerb == "share"){
	    if (timelineMain.querySelector(`[id="${item.object.id}"]`) != null){
		var itemEl = document.getElementById(item.id);
		var objEl = timelineMain.querySelector(`[id="${item.object.id}"]`);
		if (item.activityVerb == "share"){
		    timelineMain.insertBefore(objEl,itemEl);
		}
		// Place the favorite inside of the item's drawer.
		objEl.querySelector(".drawer").appendChild(itemEl);
	    }
	}
    }
    timelineMain.appendChild(nextPage);
}

function ActivityObject(e){
    // I am a Javascript object, parsed from an Atom <entry> 

    // These are universal, so we don't have to test for their existence
    this.id = e.querySelector("id").textContent;
    this.activityVerb = e.querySelector("entry > verb").textContent;
    this.published = e.querySelector("entry > published").textContent;
    this.updated = e.querySelector("entry > updated").textContent;

    // These may or may not be present, depending on the feed.

    this.permalink = (e.querySelector("entry > link[rel='alternate']")) ?
	e.querySelector("entry > link[rel='alternate']").getAttribute("href") : this.id;
    this.localId = (e.querySelector("entry > notice_info")) ?
	e.querySelector("entry > notice_info").getAttribute("local_id") :
	e.querySelector("status_net").getAttribute("notice_id");

    this.actor = {};
    this.object = {};

    this.setActor = function(a){
	try{
	this.actor.name = a.querySelector("author > name").textContent;
	this.actor.displayName = (a.querySelector("author > displayName")) ? 
	purifyText(a.querySelector("author > displayName").textContent) : "";
	//this.actor.displayName = purifyText(a.querySelector("source > title").textContent);
	this.actor.uri = a.querySelector("author > uri").textContent;
	this.actor.url = a.querySelector("author > link[rel=alternate]").getAttribute("href");
	this.actor.avatar = a.querySelector("author > link[rel=avatar]").getAttribute("href");
	} catch(e){
	    barf(e + "<br>");
	}
    };

    this.tags = [];
    if (e.querySelectorAll("category")) { 
	var tagElements = e.querySelectorAll("category");
	for (var ix = 0; ix < tagElements.length; ix++){
	    this.tags.push(tagElements.item(ix).getAttribute("term"));
	}
    }

    // Individual user timelines have the author set once under the <feed>
    // element, while other feeds have the author set per <entry>.
    if(e.querySelector("entry > author")){
	this.setActor( e.querySelector("author") );
    } else {
	// This -- perhaps stupidly -- assumes we've been passed a
	// reference to an Atom entry inside of a <feed>.
	this.setActor( e.parentElement.querySelector("author"));
    }
    
}

function objectifyMastoContext(contextObj,context,sourceServer){
    // Combine the ancestors and descendants into a big list
    var allItems = contextObj["ancestors"].concat(contextObj["descendants"]);
    var activities = [];
    for(var ix in allItems){
	var item = allItems[ix];
	var activityObject = activityObjectFromMastoObject(item,sourceServer);
	
	var replyId = item.in_reply_to_id;
	if(replyId){
	    var replyTarget = allItems.find(function(zardoz){
		return zardoz.id == this;
	    },replyId);
	    if(replyTarget != undefined){
		activityObject.replyTo = replyTarget.uri;
	    }
	}

	activities.push(activityObject);
    }
    barf("... " + activities.length + " items parsed.");
    return activities;
}

function activityObjectFromMastoObject(o,sourceServer){
    var a = {};
    a.id = o.uri;
    a.activityVerb = "post";
    a.published = o.created_at;
    a.updated = a.published;
    a.permalink = o.url;
    a.localId = o.id;

    
    a.actor = {};
    a.actor.name = o.account.username;
    a.actor.displayName = o.account.display_name;
    a.actor.uri = o.account.url;
    a.actor.url = o.account.url;
    a.actor.avatar = o.account.avatar;

    a.tags = [];
    for(var ix in o.tags){
	a.tags.push(o.tags[ix].name);
    }

    a.content = o.content;

    // Mastodon doesn't really work in an ActivityStreams sort-of way,
    // so favorites and boosts don't appear in a conversation's context?
    // Therefore, all activities from a context are a note (a "Status"
    // I think is the type they use, actually).
    
    a.object = {};
    a.object.type = "note";
    a.object.id = a.id;
    a.object.permalink = a.permalink;
    a.object.title = "";
    a.object.content = a.content;
    
    a.object.author = a.actor;
    a.object.author.summary = o.account.note;
    a.object.author.localID = o.account.id;

    if(sourceServer == domainOf(getNodeUrl())){
	a.localId = o.id;
	a.object.localId = a.localId;
    } else {
	a.localId = null;
	a.object.localId = null;
    }

    
    incidentalUsers[a.object.author.uri] = a.object.author;

    return a;
}


function objectify(atomFeed){
    // Convert <entry> items from an Atom feed to an internal
    // representation of timeline activities, and return them
    // in an array.

    // !!! This function, and the ActivityObject() constructor
    // are a nasty tangle and need to be sorted-out much better.
 
    var newTL = [];
    var atomEntries = atomFeed.querySelectorAll("feed > entry");
    var sourceServer = domainOf(atomFeed.querySelector('feed > link[rel="self"]').getAttribute("href"));

    for (var i = 0; i < atomEntries.length; i++){
	var entry = atomEntries.item(i);
	var newItem = new ActivityObject(entry);
	    	
	if (entry.querySelector("entry > content")){
	    newItem.content = entry.querySelector("entry > content").textContent;
	} else {
	    // No <content>? Probably a poll. I don't handle polls right now.
	    barf("<entry> has no <content>. Poll or other unhandled object type? (" + newItem.permalink + ")"); 
	    continue;
	};

	if (entry.querySelector("in-reply-to")){
	    newItem.replyTo = entry.querySelector("*|in-reply-to").getAttribute("href");
	};

	// The activity verb is probably in the form of
	// "http://activitystrea.ms/blah/verb", but we don't need the
	// whole namespace url, just the last bit.
	
	newItem.activityVerb = newItem.activityVerb.substring(newItem.activityVerb.lastIndexOf("/")+1);

	// Sometimes the activity object of a post is an explicit
	// <object> element, while for post/note and post/comment
	// activities, the object is implicit in the <entry>.

	var activityObject = (entry.querySelector("entry > object")) ?
	    entry.querySelector("entry > object") : entry;
	
	// If the object doesn't have a permalink, it's probably a reference to 
	// a deleted notice or something.  Just skip it and forget about it.

	if (!activityObject.querySelector("link[rel=alternate]")) continue;
	
	newItem.object = { 
	    "type" : activityObject.querySelector("object-type").textContent,
	    "id" : activityObject.querySelector("id").textContent,
	    "permalink" : activityObject.querySelector("link[rel=alternate]").getAttribute("href"),
	    "title" : activityObject.querySelector("title").textContent
	};


	if (sourceServer == domainOf( getNodeUrl() ) ) {
	    // Store the activity object's local ID if it has one.
	    newItem.object.localId = (activityObject.querySelector("status_net")) ?
		activityObject.querySelector("status_net").getAttribute("notice_id") :
		null;
	} else {
	    // If this is not a local feed, we don't want to store the
	    // activity's local ID. See flapOn()
	    newItem.localId = "";
	}


	// Some objects do not have <content>, such as group joins
	if(activityObject.querySelector("content")){
	    newItem.object.content = activityObject.querySelector("content").textContent;
	}
	
	// Aaaand, finally, some author information for the shared/favored notice:
	if(activityObject.querySelector("author")){
	    newItem.object.author = {
		"name" : activityObject.querySelector("author > name").textContent,
		"displayName" : purifyText(activityObject.querySelector("author > displayName").textContent),
		"uri" : entry.querySelector("author > uri").textContent,
		"url" : entry.querySelector("author > link[rel=alternate]").getAttribute("href"),
		"avatar" : entry.querySelector("author > link[rel=avatar]").getAttribute("href")
	    };
	    
	    newItem.object.author["summary"] = (activityObject.querySelector("author > summary")) ? activityObject.querySelector("author > summary").textContent : "";
	    newItem.object.author["localID"] = (activityObject.querySelector("author > profile_info")) ? activityObject.querySelector("author > profile_info").getAttribute("local_id") : null;
	    // We keep a record of all user accounts we've seen.
	    incidentalUsers[newItem.object.author.uri] = newItem.object.author;
	};

	// Here, too, we only want the shorthand of the activity object-type	
	newItem.object.type = newItem.object.type.substr(newItem.object.type.lastIndexOf("/")+1);    

	newTL.push(newItem);
    }
    return newTL;
}

function domainOf(url){
    if(url.indexOf("://") >= 0){
	return url.split("/")[2];
    } else {
	console.log(url + "is not a URL?");
	return url;
    }
}

function makeFOAF(foaf){
    // The XML FOAF format is... not nice to deal with, IMO. So, when we get
    // the user's FOAF, convert it into a Javascript object.
    var foafObj = [];
    //var foafObj = {};

    var acctURI = foaf.querySelector("primaryTopic").getAttribute("rdf:resource");
    console.log("Processing FOAF for " + acctURI);
    
    var accounts = foaf.querySelectorAll("account");
    // Skip the first account -- that's the user.
    for (var ix = 1; ix < accounts.length; ix++){
	var acct = accounts.item(ix);
	var nick = acct.querySelector("accountName").textContent;
	var page = acct.querySelector("accountProfilePage").getAttribute("rdf:resource");
	var uri = acct.querySelector("account_of").getAttribute("rdf:resource");
	var node = domainOf(page);
	var webFinger = nick + "@" + node;
	var acctObj = { "nickname" : nick,
			"url" : page,
			"uri" : uri,
			"node" : node,
			"followed" : false,
			"following" : false};
	//foafObj[webFinger] = acctObj;
	foafObj.push(acctObj);
    }
    // Now update each account's "followed" value to reflect whether we're
    // following them
    var agentElement = foaf.querySelector('Agent[*|about="' + acctURI + '"]');
    var follows = Array.from(agentElement.querySelectorAll("follows"));
    for(var ix in follows){
	// Find the corresponding account in acctObj
	var person = foafObj.find(function(p){
	    return (p.uri + "#acct") == this;
	},follows[ix].getAttribute("rdf:resource"));
	if(person){
	    person.followed = true;
	} else {
	    console.log("FOAF problem: couldn't match ", follows[ix]);
	}
    }
    
    // And update each account's "following" value to reflect whether they're
    // following us
    var followers = Array.from(foaf.querySelectorAll('follows[*|resource="' + (acctURI + "#acct") + '"]'));
    for (var ix in followers){
	// Get the URI of the account that follows us
	var acctOf = followers[ix].parentElement.querySelector('account_of').getAttribute("rdf:resource").split("#")[0];
	// Find that account in foafObj
	var person = foafObj.find(function(p){
	    return p.uri == this;
	},acctOf);
	if(person){
	    person.following = true;
	} else {
	    console.log("FOAF problem: couldn't find follower ", followers[ix]);
	}
    }
    
    return foafObj;
}

function itemToTimelineElement(item){

    // Render an item (from our internal representation) as a DOM fragment
    // and return it.
    
    // Some useful date stuff...
    var itemDate = new Date(item.published);
    var dateString = "" + itemDate.getFullYear() + "." + pad(itemDate.getMonth()+1,2) +
	"." + pad(itemDate.getDate(),2);
    var timeString = "" + pad(itemDate.getHours(),2) + ":" + pad(itemDate.getMinutes(),2);

    // Put all the metadata we might want in the <article> element
    var itemArticle = document.createElement("article");
    itemArticle.id = item.id;
    itemArticle.setAttribute("data-permalink",item.permalink);
    itemArticle.setAttribute("data-localId",item.localId);
    itemArticle.setAttribute("data-verb",item.activityVerb);
    itemArticle.setAttribute("data-type",item.object.type);
    itemArticle.setAttribute("data-author",item.actor.uri);

    var tagString = " ";
    for (var ix=0; ix < item.tags.length; ix++){
	tagString += item.tags[ix] + " ";
    }
    itemArticle.setAttribute("data-tags",tagString);

    if (item.replyTo) itemArticle.setAttribute("data-replyTo",item.replyTo);
    //itemArticle.setAttribute("data-replyTo",(item.replyTo) ? item.replyTo : "");

    // Universal activity header
    var itemHeader = document.createElement("header");
    itemHeader.setAttribute("class","articleHeader");

    // Actor avatar
    var itemAvatar = document.createElement("img");
    itemAvatar.setAttribute("class","avatar");
    itemAvatar.setAttribute("title","Click for info.");
    itemAvatar.src = item.actor.avatar;
    //itemHeader.appendChild(itemAvatar);
    itemArticle.appendChild(itemAvatar);
    itemAvatar.onclick = function(){
	userPopup(item.actor.uri);
    }

    // Date / Time
    var dateline = document.createElement("span");
    dateline.setAttribute("class","tlDateline");
    var dateSpan = document.createElement("span");
    dateSpan.setAttribute("class","tlDate");
    dateSpan.innerHTML = dateString;
    var timeSpan = document.createElement("span");
    timeSpan.setAttribute("class","tlTime");
    timeSpan.innerHTML = " " + timeString;
    dateline.appendChild(dateSpan);
    dateline.appendChild(timeSpan);
    itemHeader.appendChild(dateline);

    var itemAuthor = document.createElement("span");
    itemAuthor.setAttribute("class","displayName");
    var authorLink = document.createElement("a");
    authorLink.href = item.actor.url;
    authorLink.setAttribute("title",item.actor.name + '@' + domainOf(item.actor.uri));
    authorLink.innerHTML = item.actor.displayName;
    itemAuthor.appendChild(authorLink);
    itemAuthor.innerHTML += " <small>(" + authorLink.getAttribute("title") + ")</small>"
    itemHeader.appendChild(itemAuthor);

    itemArticle.appendChild(itemHeader);
    
    var itemBody = document.createElement("main");
    itemBody.setAttribute("class","itemBody");
    itemArticle.appendChild(itemBody);

    var codeString = "";

    switch (item.activityVerb) {
    case "share" :
	codeString += "&#9851; " + `<a href="${item.object.author.url}" ` +
	    'title="' + item.object.author.name + '@' +
	    domainOf(item.object.author.url) + '">' +
	    item.object.author.displayName + '</a>: ' +
	    `<blockquote>${item.object.content}</blockquote>`;
	break;
    case "favorite" :
	var titleString = item.object.title.substring(4);
	codeString += `... liked ` +
	    `a <a href="${item.object.permalink}">${titleString}</a>:` +
	    `<blockquote>${item.object.content}</blockquote>`;
	break;
    case "follow" :
	codeString += `<span id="${item.id}">[...started following ${item.object.title}]</span>`;
	break;
    case "join" :
	codeString += `<span id="${item.id}">join...</span>`;
	break;
    default :
	/*
	if (item.replyTo){
	    codeString +=`<a class="tlInReplyTo" href="javascript:jumpTo('${item.replyTo}','${item.id}')" ` +
		`title="In reply to ${item.replyTo}">&#x2B0F;</a>`;
	}
	*/
	codeString += item.content;
    }
    itemBody.innerHTML = codeString;

    if(item.replyTo){
	var upJump = document.createElement("a");
	upJump.classList.add("tlInReplyTo");
	upJump.onclick = function(a,b){
	    jumpTo(a,b);
	}.bind(upJump,item.replyTo,item.id);
	upJump.title = "In reply to " + item.replyTo;
	upJump.innerHTML = "&#x2B0F;";
	itemBody.insertBefore(upJump,itemBody.childNodes[0]);
    }
    
    // Inline "attachment" links
    var attachments = itemBody.querySelectorAll("a[class*='attachment']");
    if(attachments){ 
	for(var a = 0;a < attachments.length;a++){
	    var href = attachments.item(a).getAttribute("href");
	    // Do a HEAD request on the attachment. If the media type is
	    // image/*, audio/*, or video/*, stick an appropriate element in the 
	    // post to accommodate it. If the type is "text/html", look for
	    // openGraph tags.

	    // This needs to be done much more elegantly. However, this is
	    // surprisingly effective, crude as it is.
	    var h = new XMLHttpRequest();
	    h.onload = function(id,url){
		if(this.status == 200){
		    var type = this.getResponseHeader("content-type");
		    var i = document.getElementById(id);
		    var attachment = document.createElement("div");
		    attachment.className = "articleAttachment";
		    if (type.startsWith("image")){
			// TODO: Use a <canvas> to scale images down
			// to a manageable size so that if the timeline
			// loads for a while we don't end-up eating gigabytes 
			// of RAM on the 4+ megapixel images everyone seems to
			// post these days.
			var lnk = document.createElement("a");
			lnk.setAttribute("class","inlineImage");
			lnk.href = url;
			lnk.setAttribute("target","new");
			var img = document.createElement("img");
			img.src = url;
			lnk.appendChild(img);
			attachment.appendChild(img);
			i.querySelector("main").appendChild(attachment);
		    } else if(type.startsWith("audio")){
			// Audio stuff...
			var player = document.createElement("audio");
			player.preload = "metadata";
			player.src = url;
			player.controls = true;
			attachment.appendChild(player);
			i.querySelector("main").appendChild(attachment);
		    } else if(type.startsWith("video")){
			// Video stuff...
			var player = document.createElement("video");
			player.preload = "metadata";
			player.src = url;
			player.controls = true;
			attachment.appendChild(player);
			i.querySelector("main").appendChild(attachment);
		    } else if(type.startsWith("text/html")){
			getOembed(id,url);
		    }
		} else {
		    barf(this.status);
		}
	    }.bind(h,item.id,href);
	    h.open('HEAD',proxyString+href,true);
	    h.send();
	}
    }
    itemFooter = document.createElement("footer");
    itemFooter.setAttribute("class","openFolder");
    itemFooter.innerHTML = "&#9660;";
    itemFooter.onclick = function(){ fold(itemArticle); };
    itemArticle.appendChild(itemFooter);
    
    itemDrawer = document.createElement("div");
    itemDrawer.setAttribute("class","drawer");
    itemArticle.appendChild(itemDrawer);
    
    return itemArticle;
}
function jumpTo(url,from){
    if (from) window.location.hash = "#"+from;
    var target = document.querySelector(`article[data-permalink='${url}']`);
    if(!target){
	getActivity(permalinkToAtomUrl(url),true);
    } else {
	window.location.hash = "#" + target.id;
    }
}
function getOembed(id,url){
    // Load an HTML page, look for opengraph/Oembed tags and ...do stuff.

    // This is an extremely crude hack ATM.
    var r = new XMLHttpRequest();
    r.onload = function(id,url){
	if(this.status == 200){
	    var html;
	    try {
		html = new DOMParser().parseFromString(this.responseText,"text/html");
	    } catch(e){
		barf("<p>" + e + "</p>");
		return;
	    }

	    var article = document.getElementById(id);
	    var ogType = html.querySelector("meta[property='og:type']");
	    if (!ogType) return;

	    ogType = ogType.getAttribute("content");

	    // Okay, so we have some OpenGraph elements. Let's try to
	    // get the most common tags here:

	    var ogTitle = (html.querySelector("meta[property='og:title']")) ?
	    html.querySelector("meta[property='og:title']").getAttribute("content") :
	    "";
	    var ogDesc = (html.querySelector("meta[property='og:description']")) ?
	    html.querySelector("meta[property='og:description']").getAttribute("content") :
	    "";
	    var ogImage = html.querySelector("meta[property='og:image']");

	    var attachment = document.createElement("div");
	    attachment.className = "articleAttachment";

	    if (ogType.match(/article/)){
		if(ogImage){
		    var embedImage = document.createElement("img");
		    embedImage.src = ogImage.getAttribute("content");
		    attachment.appendChild(embedImage);		    
		}

		var embedLink = document.createElement("a");
		embedLink.href = url;
		attachment.appendChild(embedLink);

		var embedTitle = document.createElement("h3");
		embedTitle.innerHTML = ogTitle;
		embedLink.appendChild(embedTitle);

		var embedDesc = document.createElement("blockquote");
		embedDesc.innerHTML = ogDesc;
		attachment.appendChild(embedDesc);

		article.querySelector("main").appendChild(attachment);
		return;
	    } 
	    else if (ogType.match(/video/)){
		// YouTube, Vine, etc.

		// The trouble with <embed> elements is that they
		// reload every time you move them or their parent
		// elements around in the DOM. The result for us is
		// that when timeline/conversation elements are loaded
		// and merged, the <embed> collapses and reloads,
		// which makes the timeline jump all over the fucking
		// place, particularly while getSources() is fetching
		// and merging shared/favored notices...

		// So, what we'll do instead is display the og:image
		// in the article's content, which will launch the embed 
		// in a closable <div> when clicked on.

		var ogVideoUrl = html.querySelector("meta[property='og:video:url']");
		if (ogVideoUrl){
		    var embed = document.createElement("embed");
		    var embedWidth = html.querySelector("meta[property='og:video:width']");
		    var embedHeight = html.querySelector("meta[property='og:video:height']");
		    embed.setAttribute("width",embedWidth.getAttribute("content"));
		    embed.setAttribute("height",embedHeight.getAttribute("content"));
		    embed.style.clear = "both";
		    embed.src = ogVideoUrl.getAttribute("content");
		}
		if (ogImage){
		    var image = document.createElement("img");
		    image.src = ogImage.getAttribute("content");
		    image.style.cursor = "pointer";
		    image.title = "Click to show video <embed>";
		    image.onclick = function(){
			makeFloater().appendChild(embed);
		    }.bind(embed);
		    attachment.appendChild(image);
		} else if (ogTitle){
		    
		}
		article.querySelector("main").appendChild(attachment);
		return;
	    }
	    else if (ogType.match(/song/)){
		var embedUrl;
		var embedWidth;
		var embedHeight;
		if (html.querySelector("meta[property='og:video']")){
		    // BandCamp?
		    embedUrl = html.querySelector("meta[property='og:video']");
		    embedWidth = html.querySelector("meta[property='og:video:width']");
		    embedHeight = html.querySelector("meta[property='og:video:height']");
		} else if (html.querySelector("meta[property='twitter:player']")){
		    // Soundcloud?
		    embedUrl = html.querySelector("meta[property='twitter:player']");
		    embedWidth = html.querySelector("meta[property='twitter:player:width']");
		    embedHeight = html.querySelector("meta[property='twitter:player:height']");
		} else {
		    return;
		}

		var embed = document.createElement("embed");
		embed.setAttribute("width",embedWidth.getAttribute("content"));
		embed.setAttribute("height",embedHeight.getAttribute("content"));
		embed.src = embedUrl.getAttribute("content");
		attachment.appendChild(embed);
		article.querySelector("main").appendChild(attachment);
		return;
	    } 
	    else {
		oembedImage = html.querySelector("meta[property='og:image']");
		if(oembedImage){		
		    var lnk = document.createElement("a");
		    lnk.setAttribute("class","inlineImage");
		    lnk.href = url;
		    lnk.setAttribute("target","new");
		    var img = document.createElement("img");
		    img.src = oembedImage.getAttribute("content");
		    lnk.appendChild(img);
		    attachment.appendChild(lnk);
		    article.querySelector("main").appendChild(attachment);
		}
		return;
	    }
	}
    }.bind(r,id,url);
    r.open('GET',proxyString+url,true);
    r.send();
}

function fold(article){
    
}

function extInfo(item){
    // Add extended information to the notice's header. 'item' should be an
    // <article class="timelineArticle">

    // If the extInfo is already visible, remove it.
    if(item.querySelector(".extInfo")){
	item.querySelector("header").removeChild(item.querySelector(".extInfo"));
    } else {
	var eInfo = document.createElement("div");
	eInfo.setAttribute("class","extInfo");
	var codeString = 'Global ID: ' + item.getAttribute("id") + '<br>' +
	    'Local ID: <a href="' + 
	    getNodeUrl() + '/notice/' + item.getAttribute("data-localId") + 
	    '" class="localId" title="local ID">' + item.getAttribute("data-localId") + 
	    '</a><br>' +
	    'Permalink: <a class="permalink" href="' + item.getAttribute("data-permalink") + 
	    '">' + item.getAttribute("data-permalink") + '</a><br>' +
	    'Verb: ' + item.getAttribute("data-verb") + '<br>' +
	    'Object-type: ' + item.getAttribute("data-type") + '<br>' +
	    '</div>';
	eInfo.innerHTML = codeString;
	item.querySelector("header").appendChild(eInfo);
    }
}

function findById(el,ix,ar){
    if (el.id == this) {
	return true;
    }
}

function userInit(){
    var gsUser = localStorage.GNUsocialUser;
    var gsServer = getNodeUrl();

    document.getElementById("userWebfingerName").innerHTML = "Logging-in " + gsUser + "...";
    // Get the user's FOAF document
    
    var s = new XMLHttpRequest();
    s.onload = function (){
	if (this.status === 200){
	    foaf = this.responseXML;
	    var userInfoDiv = document.getElementById("userInfo");
	    var loginFormDiv = document.getElementById("loginForm");
	    var displayNameA = document.getElementById("userDisplayName");
	    var webfingerA = document.getElementById("userWebfingerName");

	    //userInfoDiv.style.display = "block";
	    //loginFormDiv.style.display = "none";
	    document.getElementById('settings').style.display = "none";

	    var rdfAgent = foaf.querySelector("RDF > Agent");

	    displayNameA.textContent = rdfAgent.querySelector("name").textContent;
	    displayNameA.href = rdfAgent.querySelector("accountProfilePage").getAttribute("rdf:resource");
	    webfingerA.textContent = localStorage.GNUsocialUser + "@" + localStorage.GNUsocialNode;
	    webfingerA.href = displayNameA.href;
	    foaf = makeFOAF(foaf);
	}
    }.bind(s);

    // FOAF requests don't play nice with CORS, so for now we have to
    // run 'em through a proxy.

    var reqUrl = proxyString + gsServer + "/" + gsUser + "/foaf";
    
    s.open('GET',reqUrl,true);
    s.send();

    // Get the user's service document
    var m = new XMLHttpRequest();
    m.onload = function(){
	if(this.status === 200){
	    sdoc = this.responseXML;
	    url = sdoc.querySelector("collection[href*='membership']").getAttribute("href");
	    
	    localUserID = url.split("/");
	    localUserID = localUserID[localUserID.length-1].split(".")[0];
	    getMemberships(url);
	}
    }
    m.open('GET',proxyString + 
	   gsServer + "/api/statusnet/app/service/" + gsUser + ".xml",
	   true);
    m.send();

    // Grab the user's home timeline to start things off.
    
    getTimeline("home");
}

function getMemberships(url){
    var m = new XMLHttpRequest();
    m.onload = function(){
	if(this.status === 200){
	    var entries = this.responseXML.querySelectorAll("entry");
	    memberships = [];
	    for (var ix = 0;ix < entries.length; ix++){
		var entry = entries.item(ix);
		var group = {
		    "id" : entry.querySelector("object > id").textContent,
		    "name" : entry.querySelector("object > preferredUsername").textContent,
		    "displayName" : entry.querySelector("object > displayName").textContent
		};
		memberships.push(group);
		//barf(memberships[ix].name + " " + memberships[ix].id + "<br>");
	    }
	}
	else {
	    barf("<strong>Unable to load group memberships: HTTP </strong>" + 
		 this.status + "<br>");
	}
    }
    m.open('GET',url,true);
    //m.open('GET',proxyString + url,true);
    m.send();
}

function oauthSignIn(){
    // Ask for a Request token from the user's server. 

    applySettings();
    var gsServer = getNodeUrl();
    
    // Build the query string for a Request Token
    var queryString = "oauth_callback=" +
	encodeURIComponent("http://aegn.us") +
	"&oauth_consumer_key=" + localStorage.aegnusCKey +
	"&oauth_nonce=" +
	Math.random().toString(36).replace(/[^a-z]/, '').substr(2) +
	"&oauth_signature_method=HMAC-SHA1" +
	"&oauth_timestamp=" + parseInt(Date.now()/1000);
    
    // The tricky bit here is to make sure that the query string gets
    // percent-encoded, too.
    var baseString = "POST&" +
	encodeURIComponent(gsServer +"/api/oauth/request_token") + "&" +
	encodeURIComponent(queryString);

    // Note that the key is cSecret plus ampersand and nothing else 
    var sig = CryptoJS.HmacSHA1(baseString,localStorage.aegnusCSecret+"&");
    sig = encodeURIComponent(sig.toString(CryptoJS.enc.Base64));

    var r = new XMLHttpRequest();
    r.onload = function(gsServer){
	// *** ADD SOME ERROR-CHECKING HERE, ADAM ***

	// If all goes well, we get back a Request token (oauth_token)
	// and a Request token Secret (oauth_token_secret) for signing
	// the Stage 3 request.
	
	var resp = this.responseText.trim().split("&");
	localStorage.OAReqToken = resp[0].substr(resp[0].indexOf("=")+1);
	localStorage.OAReqSecret = resp[1].substr(resp[1].indexOf("=")+1);

	//alert("Got this back:\n\n" + this.responseText);

	oauthStageTwo(localStorage.OAReqToken,gsServer);
	    
    }.bind(r,gsServer);
    r.open('POST',gsServer + "/api/oauth/request_token?" +
	   queryString + "&oauth_signature=" + sig , true);
    r.send();
}

function oauthStageTwo(token,gsServer){
    // Nothing too special, here. We have a request token and secret,
    // and we need to send the user to their server to authorize the
    // app.  After they come back, they'll be dropped into
    // oauthStageThree()

    location = gsServer + "/api/oauth/authorize?oauth_token=" + token;
    
}

function oauthStageThree(token,verifier){
    // User has authorized the application on their server. We've been
    // returned a token (which should match the OAReqToken in localStorage)
    // and an oauth_verifier string which needs to be sent with our request for
    // an Access token. Let's see how it goes!

    if (token != localStorage.OAReqToken) {
	alert("Returned token does not match stored token. Aborting.");
	return;
    }

    var gsServer = (localStorage.GNUsocialNodeSSL ? "https://" : "http://") +
	localStorage.GNUsocialNode;
    
    // Remember, query parameters *IN ALPHABETICAL ORDER*
    var queryString = "oauth_consumer_key=" + localStorage.aegnusCKey +
	"&oauth_nonce=" +
	Math.random().toString(36).replace(/[^a-z]/, '').substr(2) +
	"&oauth_signature_method=HMAC-SHA1" +
	"&oauth_timestamp=" + parseInt(Date.now()/1000) +
	"&oauth_token=" + token +
	"&oauth_verifier=" + verifier;
    
    var baseString = "POST&" +
	encodeURIComponent(gsServer + "/api/oauth/access_token") + "&" +
	encodeURIComponent(queryString);

    var sig = CryptoJS.HmacSHA1(baseString,
				localStorage.aegnusCSecret + "&" + localStorage.OAReqSecret);
    sig = encodeURIComponent(sig.toString(CryptoJS.enc.Base64));
    
    var r = new XMLHttpRequest();
    r.onload = function(){
	// *** AGAIN, ADD SOME ERROR-CHECKING, ADAM. ***

	// Did we get back an Access token?! I sure hope so!
	
	if (this.responseText.indexOf("oauth_token") >= 0){
	    var resp = this.responseText.trim().split("&");
	    localStorage.OAAccessToken = resp[0].substr(resp[0].indexOf("=")+1);
	    localStorage.OAAccessSecret = resp[1].substr(resp[1].indexOf("=")+1);
	    //alert(this.responseText); 
	    userInit();
	}
	else {
	    alert("DID NOT WORK, MILLIE.\nTHIS IS WHAT WE GOT:\n" +
		  this.responseText); }
	
    }.bind(r);
    r.open('POST',gsServer + "/api/oauth/access_token?" +
	   queryString + "&oauth_signature=" + sig , true);
    r.send();
}

function signRequest(method,url,head,tail){
    if (!head) { head = "";}
    if (!tail) { tail = "";}
    var queryString = head + "oauth_consumer_key=" + localStorage.aegnusCKey +
	"&oauth_nonce=" +
	  Math.random().toString(36).replace(/[^a-z]/,'').substr(2) +
	"&oauth_signature_method=HMAC-SHA1" +
	"&oauth_timestamp=" + parseInt(Date.now()/1000) +
	"&oauth_token=" + localStorage.OAAccessToken + tail;
    var baseString = method + "&" +
	encodeURIComponent(url) + "&" +
	encodeURIComponent(queryString);
    var sig = CryptoJS.HmacSHA1(baseString,
				localStorage.aegnusCSecret + "&" + localStorage.OAAccessSecret);
    sig = encodeURIComponent(sig.toString(CryptoJS.enc.Base64));
    return `${queryString}&oauth_signature=${sig}`;
}

// UI Crap ....
// ---------------------------------------------------------
function settingsToggle(){
    var settings = document.getElementById("settings");
    if (settings.style.display){
	settings.style.display = (settings.style.display == "none") ? "block" : "none";
    } else {
	settings.style.display = "block";
	window.location.hash = "#settings";
    }
}
function filterFlap(){
    var flap = document.getElementById("filterArea");
    if(flap.style.display){
	flap.style.display = (flap.style.display == "block") ? "none" : "block";
    } else {
	flap.style.display = "block";
    }
}

function flapOn(id){
    // Open an operations flap on an article/activity

    var target = document.getElementById(id);

    toolFlap.innerHTML = "";
    toolFlap.target = target;
    toolFlap.style.display = "block";
    target.insertBefore(toolFlap,target.firstElementChild);

    // Create the buttons

    // These only work if the post locally cached
    if(target.getAttribute("data-localId") != ""){
	var like = document.createElement("input");
	like.type = "button";
	like.value = "LIKE";
	like.onclick = function(){
	    favorPost(target.getAttribute("data-localId"),
		      target.getAttribute("data-permalink"));
	};
	toolFlap.appendChild(like);

	var rep = document.createElement("input");
	rep.type = "button";
	rep.value = "REPLY";
	rep.onclick = function(){
	    reply(target.getAttribute("id"));
	};
	toolFlap.appendChild(rep);

	var share = document.createElement("input");
	share.type = "button";
	share.value = "SHARE";
	share.onclick = function(){
	    share(target.getAttribute("id"));
	};
	toolFlap.appendChild(share);

	if((getNodeUrl() + "/user/" + localUserID) == target.getAttribute("data-author")){
	    var del = document.createElement("input");
	    del.type = "button";
	    del.value = "DELETE";
	    del.onclick = function(){
		deletePost(target.getAttribute("id"));
	    };
	    toolFlap.appendChild(del);
	}
    }

    var info = document.createElement("input");
    info.type = "button";
    info.value = "HEADERS";
    info.onclick = function(){
	extInfo(target);
    };

    var readButton = document.createElement("input");
    readButton.type = "button";
    readButton.value = "FETCH CONVERSATION";
    readButton.onclick = function(){
	// Alright, this is a bit funky, but we want to start the
	// conversation scan from our home node instead of from the
	// notice's actual permalink, if possible, that way we can get
	// the local ID of notices and make them actionable.
	var a = document.getElementById(id);
	loadConversation(getNodeUrl() + "/notice/" +
			 a.getAttribute("data-localId"));
    }

    toolFlap.appendChild(info);
    toolFlap.appendChild(readButton);

}

function loadConversation(postUrl){
    // Clear the conversation viewing panel, barfdiv, infodiv...
    if(convoDiv.querySelector("article")){
	returnItemsToTimeline();
    }
    convoDiv.innerHTML = "";
    barfDiv.innerHTML = "";
    infoDiv.style.display = "none";
    barf("CONVERSATION MANIFEST<br><br>")


    // Fetch/build the conversation
    conversationFromNoticePermalink(postUrl);
}

function makeToolFlap(){
    var flap = document.createElement("div");
    flap.id = "toolFlap";
    flap.style.display = "none";
    document.body.appendChild(flap);
    return flap;
}
function userPopup(uri){
    var timelineURL = uri.replace("user/","api/statuses/user_timeline/") + ".atom";
    var uInfo = document.createElement("div");
    uInfo.setAttribute("class","userInfoCard");
    uInfo.innerHTML = "Getting information for: " + uri + "...";
    var uPop = makeFloater();
    uPop.appendChild(uInfo);
    if(incidentalUsers[uri]){
	var u = incidentalUsers[uri];
	var uid = u.localID;
	uInfo.innerHTML = '<figure><img src="' + u.avatar + '"></figure>' +
	    "<h1>" + u.displayName + "</h1>" +
	    '<h2><a target="_NEW" href="' + u.url + '">' + u.url + '</a></h2>' +
	    "<p style=\"white-space: pre-wrap\">" + u.summary + "</p>";
	
	var tlLink = document.createElement("a");
	tlLink.innerHTML = "[view timeline]";
	tlLink.onclick = function(url,floater){
	    getTimeline(url);
	    floater.remove();
	}.bind(tlLink,timelineURL,uPop);
	uInfo.appendChild(document.createElement("p").appendChild(tlLink));
	
	// If we're subscribed, offer an unsubscribe button. If we're not, offer a
	// subscribe button.
	var uObject = foaf.find(function(p){
	    return p.uri == this;
	},uri);
	if(uObject){
	    if(uObject.following){
		// User follows us. Give some informative text to that effect.
		var subInfo = document.createElement("p");
		uPop.appendChild(subInfo);
		subInfo.innerHTML = "<i>" + u.displayName + " is subscribed to you.</i>";
	    }
	    if(uObject.followed){
		// We follow them. Give us an "unsubscribe" button.
		var unsubButton = document.createElement("input");
		unsubButton.setAttribute("type","button");
		unsubButton.value = "unsubscribe";
		unsubButton.onclick = function(){
		    //unsubUser(uri);
		    unsubUser(uid);
		}
		uPop.appendChild(unsubButton);
	    }
	} else {
	    // User isn't in our FOAF, so there's no subscription relationship.
	    // Make a 'subscribe' button.
	    var subButton = document.createElement("input");
	    subButton.setAttribute("type","button");
	    subButton.value = "subscribe";
	    if(!uid){
		// If we don't have a local ID for the user, we just have to
		// go to their profile page on their node.
		subButton.value += " remotely";
		subButton.onclick = function(){
		    window.open(u.url,"_NEW");
		}
	    } else {
		subButton.onclick = function(){
		    //subUser(uri);
		    subUser(uid);
		}
	    }
	    uPop.appendChild(subButton);
	    console.log("Did I get here?");
	}
    }
}
function subUser(uri){
    // Sub using the Twitter-ish API, because it works.
    // Instead of a URI (for the non-working APP version below), pass me
    // an instance-local ID.
    var xhr = new XMLHttpRequest();
    xhr.onload = function(){
	if (this.status === 200){
	    convoDiv.innerHTML = "";
	    barf(this.responseText); }
	else if (this.status === 201){
	    // ALL GOOD!
	    barf("User followed:<br>" + purifyText(this.responseText));
	    getTimeline();
	} else {
	    alert(this.status + "\n" + this.responseText);
	}
    }.bind(xhr);
    outboxURL = getNodeUrl() + "/api/friendships/create.json";
    xhr.open("POST",outboxURL + "?" + signRequest("POST",outboxURL,"","&user_id="+uri),true);
    xhr.setRequestHeader("Accept","*");
    xhr.setRequestHeader("content-type","text/plain; charset=utf-8");
    xhr.send(post);

    /* 
       APP version, here, but it doesn't work. Keeping it around just in case.
    var post = atomPostBody();
    var entry = post.querySelector("entry");
    var object = post.createElementNS("http://activitystrea.ms/spec/1.0/","object");
    var objectType = post.createElementNS("http://activitystrea.ms/spec/1.0/","object-type");
    objectType.appendChild(post.createTextNode("person"));
    object.appendChild(objectType);
    entry.appendChild(object);

    var verb = post.createElementNS("http://activitystrea.ms/spec/1.0/","verb");
    verb.appendChild(post.createTextNode("follow"));
    entry.appendChild(verb);

    var uid = post.createElement("id");
    uid.appendChild(post.createTextNode(uri));
    entry.appendChild(uid);

    // That SHOULD be all we need.

    var xhr = new XMLHttpRequest();
    xhr.onload = function(){
	if (this.status === 200){
	    convoDiv.innerHTML = "";
	    barf(this.responseText); }
	else if (this.status === 201){
	    // ALL GOOD!
	    barf("User followed:<br>" + purifyText(this.responseText));
	    getTimeline();
	} else {
	    alert(this.status + "\n" + this.responseText);
	}
    }.bind(xhr);
    outboxURL = getNodeUrl() + "/api/statusnet/app/subscriptions/" +
	localUserID + ".atom";
    xhr.open("POST",outboxURL + "?" + signRequest("POST",outboxURL),true);
    xhr.setRequestHeader("Accept","*");
    xhr.setRequestHeader("content-type","text/plain; charset=utf-8");
    xhr.send(post);
    */
}
function unsubUser(uri){
    // Unsubscribe using the Twitter-ish API, because it works.
    var xhr = new XMLHttpRequest();
    xhr.onload = function(){
	if (this.status === 200){
	    convoDiv.innerHTML = "";
	    barf(this.responseText); }
	else if (this.status === 201){
	    // ALL GOOD!
	    barf("User UNfollowed:<br>" + purifyText(this.responseText));
	    getTimeline();
	} else {
	    alert(this.status + "\n" + this.responseText);
	}
    }.bind(xhr);
    outboxURL = getNodeUrl() + "/api/friendships/destroy.json";
    xhr.open("POST",outboxURL + "?" + signRequest("POST",outboxURL,"","&user_id="+uri),true);
    xhr.setRequestHeader("Accept","*");
    xhr.setRequestHeader("content-type","text/plain; charset=utf-8");
    xhr.send(post);
}

function subscriptionsEditor(){
    var subsDiv = document.createElement("pre");
    subsDiv.innerHTML = JSON.stringify(foaf);
    var subsPop = makeFloater();
    subsPop.appendChild(subsDiv);
}
function makeFloater(){
    var floater = document.createElement("div");
    floater.className = "floater";
    floater.id = nonce();
    var closeButton = document.createElement("input");
    closeButton.setAttribute("type","button");
    closeButton.value = "X";
    closeButton.style.cssText = "display: block; float: right;";
    closeButton.onclick = function(){ nukeId(this.id) }.bind(floater);
    floater.appendChild(closeButton);
    document.body.appendChild(floater);
    return floater;
}

function nukeId(id){
    document.getElementById(id).remove();
}

function pickUp(thing){
    //    document.body.appendChild(thing);
    rect = thing.getBoundingClientRect();
    thing.style.top = rect.top + "px";
    thing.style.left = rect.left + "px";
    thing.style.position = "fixed";
    dragObject.article = thing;
}

// EVENT HANDLERS ====================

function clickHandler(e){
    // Show a toolflap if a header is clicked, dismiss the toolFlap if
    // cicked anywhere else.

    if (e.target.className == "articleHeader" &&
	toolFlap.style.display == "none"){
	flapOn(e.target.parentElement.id);
    } else {
	toolFlap.style.display = "none";
    }

    

    // open/close an article's drawer
    if (e.target.tagName.toLowerCase() == "footer"){
	if (!e.target.getAttribute("class")) { 
	    return;
	} else {
	    var targetClass = e.target.getAttribute("class");
	}

	if (targetClass == "openFolder"){
	    e.target.setAttribute("class","shutFolder");
	    e.target.innerHTML = "&#9654;"; // Right-pointing triangle
	    e.target.nextSibling.style.display = "none";
	} else if (targetClass == "shutFolder"){
	    e.target.setAttribute("class","openFolder");
	    e.target.innerHTML = "&#9660;"; // Down-pointing triangle
	    e.target.nextSibling.style.display = "block";
	}
    }
    else if (e.target.tagName.toLowerCase() == "a"){
	var a = e.target;
	if (a.className == "permalink"){
	    e.preventDefault();
	    e.stopPropagation();
	    var cd = document.getElementById("convoArea");
	    cd.style.top = e.target.offsetTop + "px";
	    cd.style.left = (e.target.offsetLeft + e.target.offsetWidth) + "px";
	    loadConversation(a.getAttribute("href"));
	}
    }
}

function overHandler(e){    
}

function outHandler(e){
    return;
}
function upHandler(e){
    dragObject.article = null;
}
function downHandler(e){
    if (e.target.id == "editorAction"){
	e.preventDefault();
	e.stopPropagation();
	pickUp(document.getElementById("postEditor"));
    } else if (e.target.className == "floater"){
	e.preventDefault();
	e.stopPropagation();
	pickUp(e.target);
    }
}
function pointInRect(point,rect){
    if(point.x <= rect.left &&
       point.x >= rect.right &&
       point.y >= rect.top &&
       point.y <= rect.bottom){
	return true;
    } else {
	return false;
    }
}
function getParentDiv(el){
    var p = el;
    while(p &&
	  p != timelineDiv &&
	  p != convoDiv){
	p = p.parentElement;
    }
    if(!p){
	return false;
    } else {
	return p;
    }

}

function scrollHandler(e){
    // Request more timeline elements when the "Loading more posts..." div scrolls
    // into view.
    if(nextPage.getBoundingClientRect().top < window.innerHeight){
	if(!fetching){
	    nextPage.innerHTML = "Loading more activities...";
	    getNextPage();
	}
    }
}

function moveHandler(e){
    if(dragObject.article != null){
	var item = dragObject.article;
	item.style.left = (parseInt(item.style.left) + (e.clientX - dragObject.lastX)) + "px";
	item.style.top = (parseInt(item.style.top) + (e.clientY - dragObject.lastY)) + "px";
    }
    dragObject.lastX = e.clientX;
    dragObject.lastY = e.clientY;
}

// UTILITY FUNCTIONS ==================

function idCompare(a,b){
    if (a.id < b.id)
	return 1;
    else if (a.id > b.id)
	return -1;
    else 
	return 0;
}

function purifyText(s){
    textPurifier.textContent = s;
    return textPurifier.innerHTML;
}

function pad(num,size){
    var s = num+"";
    while(s.length < size) s = "0" + s;
    return s;
}

function nonce(){
    return Math.random().toString(36).replace(/[^a-z]/,'').substr(2);
}

function filterToggle(f){
    filters[f].sheet.disabled = (filters[f].sheet.disabled) ? false : true;
}

function assignClickAction(id,func){
    try{
	document.getElementById(id).onclick = func;
    } catch(e){
	console.log(e);
    }
}
function colorCycle(){
    cycleColors.push(cycleColors.shift());
    cycleRule.style.background="linear-gradient(90deg," + cycleColors.toString() + ")";
    setTimeout(colorCycle,100);
}
// ========================================================================
// I N I T . . .


// Nuke the document body and rebuild it.
var initXhr = new XMLHttpRequest();
initXhr.open('GET',chrome.extension.getURL("template.html"),false);
initXhr.send();
var bodyDoc = initXhr.responseText;
document.body.innerHTML = bodyDoc;

// Some document Elements...
var timelineMain = document.getElementById('mainTimeline');
var timelineTitle = document.getElementById('timelineTitle');
var convoDiv = document.getElementById('convoDiv');
var timelineDiv = document.getElementById('timelineDiv');
    
// Some handy globals...
var currentTimeline = "";
var currentTimelinePage = 1;
var timelineRefresh;
var fetching = false;
var tlItems = []; // Internal representation of activities in the timeline
var foaf = ""; // Addressbook
var incidentalUsers = {};
var memberships; // Array of group memberships
var localUserID; // Numerical user ID

var dragObject = { "article" : null,
		   "lastX" : 0,
		   "lastY" : 0};
var cycleColors = ["#D00","#000","#B0B","#00D","#0BB","#0D0","#BB0"];
var cycleSheet = document.createElement("style");
cycleSheet.type = "text/css";
cycleSheet.textContent = ".colorcycle {background: linear-gradient(red, green, blue);}";
document.head.appendChild(cycleSheet);
cycleSheet.disabled = false;
var cycleRule = cycleSheet.sheet.cssRules[0];
colorCycle();

// Generate some page elements
var textPurifier = document.createElement("textarea");
var toolFlap = makeToolFlap();
var nextPage = document.createElement("div");
nextPage.id = "more";
nextPage.classList.add("colorcycle");
nextPage.onclick = function(){getNextPage()};
nextPage.innerHTML=`Click here to load more activities.`;

var filters = {}
filters["nsfw"] = document.createElement("style");
filters["nsfw"].innerHTML = "#mainTimeline article[data-tags*=' nsfw '] .articleAttachment{display:none;}";
document.body.appendChild(filters["nsfw"]);
filters["nsfw"].sheet.disabled = false;

filters["avatars"] = document.createElement("style");
filters["avatars"].innerHTML = "#mainTimeline article .avatar{display:none;}";
document.body.appendChild(filters["avatars"]);
filters["avatars"].sheet.disabled = true;

filters["attachments"] = document.createElement("style");
filters["attachments"].innerHTML = "#mainTimeline article .articleAttachment{display:none;}";
document.body.appendChild(filters["attachments"]);
filters["attachments"].sheet.disabled = true;

filters["replies"] = document.createElement("style");
filters["replies"].innerHTML = "#mainTimeline article[data-replyTo]{display:none;}";
document.body.appendChild(filters["replies"]);
filters["replies"].sheet.disabled = true;

// Setup some actions for the UI elements.
assignClickAction("settingsButton", function(){settingsToggle()});
assignClickAction("tlHome", function(){getTimeline('home')});
assignClickAction("tlNode", function(){getTimeline('public')});
assignClickAction("tlNet", function(){getTimeline('network')});
assignClickAction("tlMe", function(){getTimeline('outbox')});
assignClickAction("tlMentions", function(){getTimeline('replies')});
assignClickAction("tlFaves", function(){getTimeline('favorites')});
assignClickAction("postNav", function(){compose('note')});
assignClickAction("oauthSignIn", function(){oauthSignIn()});
assignClickAction("applySettings", function(){applySettings()});
assignClickAction("filterFlapToggle", function(){filterFlap();});
assignClickAction("searchButton",function(){getTimeline('search')});
assignClickAction("showComments",function(){filterToggle('replies')});
assignClickAction("showAttachments",function(){filterToggle('attachments')});
assignClickAction("showAvatars",function(){filterToggle('avatars')});
assignClickAction("showAdult",function(){filterToggle('nsfw')});
assignClickAction("showSubsButton",function(){subscriptionsEditor()});

// If the user's been here before, load their settings from localStorage
if (localStorage.GNUsocialUser){
    loadSettings();
}

// Now that this is a Web Extension, CORS proxying is unnecessary. 
var proxyString = "";
//var proxyString = (localStorage.aegnusProxy) ? 
//    localStorage.aegnusProxy : "http://crossorigin.me/";

// === Are we returning from OAuth stage 2?
if(location.search.indexOf("&oauth_verifier") >= 0 ){
    var qstring = location.search.substr(1).split("&");
    var token = qstring[0].substr(qstring[0].indexOf("=")+1);
    var verifier = qstring[1].substr(qstring[1].indexOf("=")+1);
    oauthStageThree(token,verifier);
}

// === Are we already logged-in with OAuth?
if(localStorage.OAAccessToken){
    userInit();
}
    
document.addEventListener("click",clickHandler,true);
document.addEventListener("mouseup",upHandler,true);
document.addEventListener("mousedown",downHandler,true);
document.addEventListener("mouseover",overHandler,true);
document.addEventListener("mouseout",outHandler,true);
//document.addEventListener("wheel",scrollHandler,true);
document.addEventListener("scroll",scrollHandler,true);
document.addEventListener("mousemove",moveHandler,true);

/* convo.js, part of:
   ÆGNUS:  A GNU social Client  <http://aegn.us/>
   WebExtension version.

   This is free software. It is distributed under the 
   GNU General Public License. <https://www.gnu.org/copyleft/gpl.html>
*/

// TODO:
// MAKE THE CONVERSATION ASSEMBLY ASYNCHRONOUS, ADAM

/* To make this work with Mastodon:

   - <{server}/api/v1/statuses/{id}> for an individual JSON item
   - <{server}/api/v1/statuses/{id}/context> for the JSON conversation
     + Need a parser for this

*/

var convoDiv = document.getElementById('convoArea');
var infoDiv = document.getElementById('infoArea');
var postURL = document.getElementById('postURL');
var barfDiv = document.getElementById('barfArea');
var convo;

function conversationFromNoticePermalink(postUrl){
    // This is the main function to get things rolling. Pass me the
    // URL of any GNU social notice, and I'll hunt down the rest of
    // the conversation.
    convo = {"convosChecked" : [],
	     "nodesChecked" : [],
	     "items": []};
		
    mergeConvoOfPost(postUrl,convo);
}

function serverFromURL(url){
    //
    var splitUrl = url.trim().split("/");
    var baseUrl = splitUrl[0] + "//" + splitUrl[2];
    return baseUrl;
}

function identServer(url){
    // Figure out what kind of server is serving the passed permalink
    
    var serverUrl = serverFromURL(url);
    var xhr = new XMLHttpRequest();

    barf("Checking server type...");
    
    // GNU social?  /api/statusnet/config.json
    barf("GNU social/StatusNet?");
    xhr.open('HEAD',serverUrl + '/api/statusnet/config.json',false);
    xhr.send();
    if(xhr.status === 200){
	barf("YES.");
	return "GS";
    }
    barf("no.");
    
    // Mastodon?
    barf("Mastodon?");
    xhr.open('HEAD',serverUrl + '/api/v1/instance',false);
    xhr.send();
    if(xhr.status === 200){
	barf("YES.");
	return "Mastodon";
    }
    barf("no.");
    
    // Hubzilla
    // Friendica?
    // Diaspora?

    // ... unknown.
    return false;
}

function mergeConvoOfPost(postUrl,conversation){
    // Pass me the permalink URL of a post, and I will retrieve it; then
    // I'll get the activity streams conversation that it's a part of on
    // its server and merge it with the conversation object you've passed
    // me.

    var serverType = identServer(postUrl);
    if(!serverType){
	barf("Unknown server type! Aborting.");
	return;
    }

    var postJsonUrl = postPermalinkToJsonUrl(postUrl,serverType);
    
    if (serverType == "Mastodon"){
	// If this is a Mastodon server, getting the conversation is
	// a piece of cake.

	barf("Fetching context for post: " + postJsonUrl);
	var xhr = new XMLHttpRequest();
	xhr.onload = function(postUrl,convo){
	    // Record that we've checked this node
	    var splitPostUrl = postUrl.trim().split("/");
	    convo.nodesChecked.push(splitPostUrl[2]);

	    // ----
	    // DO STUFF.
	    if (this.status == 200){
		barf("Parsing context from Mastodon server...");
		mergeConvo(
		    objectifyMastoContext(JSON.parse(this.responseText),domainOf(postUrl)),
		    convo
		);
	    } else {
		barf("...failure rec'ing context!", this);
		// Err.
	    }
	    // ----
	    
	    // Continue the search!
	    doNextPost(convo);
	}.bind(xhr,postUrl,conversation);
	xhr.open("GET",postJsonUrl + "/context",true);
	xhr.send();
	
    } else if(serverType == "GS"){
	// For GNU social we have to jump through a few hoops.
    
	// *** Currently, this relies on the JSON representation of posts,
	// because the node-local conversation ID is missing from the Atom
	// representation. ***
	
	barf("Fetching post: " + postJsonUrl);
	
	var proxiedPostJsonUrl = proxyString + postJsonUrl;
	
	var fetcher = new XMLHttpRequest();
	fetcher.onload = function(postUrl,convo){
	    // Regardless of the result, we DID check this node.
	    var splitPostUrl = postUrl.trim().split("/");
	    convo.nodesChecked.push(splitPostUrl[2]);
	    if (fetcher.status === 200){
		var postJson;
		try{
		    postJson = JSON.parse(this.responseText); 
		}
		catch(err){
		    barf("ERROR: Response is not parseable JSON.");
		}
		if (postJson){ // Valid JSON; but is it what we want?
		    if (postJson.error){ 
			barf("...no go. Friendica hates us."); // See [1]
			doNextPost(convo);
		    } else { // Success! Now start grabbing the conversation.
			barf(" ...local convo ID is " + postJson.statusnet_conversation_id + ".");
			var asConvoUrl = splitPostUrl[0] + "//" + splitPostUrl[2] +
			    "/api/statusnet/conversation/" + postJson.statusnet_conversation_id +
			    ".atom";	    
			fetchGSConvoToMerge(asConvoUrl,convo);
		    }
		} else {
		    doNextPost(convo);
		}
	    } else {
		// If we didn't get a response we can use, just jump ahead to
		// auditConvo and see if we can carry on.
		barf("HTTP " + this.status + " — Okay, let's forget about that one...");
		doNextPost(convo);
            }
	}.bind(fetcher,postUrl,conversation);
	fetcher.open('GET',proxiedPostJsonUrl,true);
	fetcher.send();
	// ---------------------------------------------------------------
	// [1] Some Friendica instances won't let us at the API without
	// authentication.  They're nice enough to give us an error in the
	// form of a JSON response though.
    } else {
	barf("Unsupported server type.");
	doNextPost(convo);
    }
}
	
function postPermalinkToJsonUrl(permalink,serverType){
    // Give me a post's permalink URL and I will return to you the
    // URL of its JSON representation.
    
    var splitPostUrl = permalink.trim().split("/");
    
    switch(serverType){
    case "GS":
	return splitPostUrl[0] + "//" + splitPostUrl[2] +
	    "/api/statuses/show/" + splitPostUrl[splitPostUrl.length-1] + ".json";
	break;
    case "Mastodon":
	console.log("postPermalinkToJsonUrl --> ", permalink );
	return splitPostUrl[0] + "//" + splitPostUrl[2] +
	    "/api/v1/statuses/" + splitPostUrl[splitPostUrl.length-1];
	break;
    default:
	return false;
    }
}


function fetchGSConvoToMerge(convoUrl,convo){
    // Pass me the URL of an ActivityStreams ATOM conversation, and I will
    // retrieve it and merge it with the conversation object you've
    // passed.


    var proxiedConvoUrl = proxyString + convoUrl;
    var convoEntries = [];
    var convoPage = 1;
    var morePages = false;
    var fetcher = new XMLHttpRequest();

    // The API only gives 20 items, so we need... a do...while loop to go
    // over multiple pages of conversation?

    // TODO: DO THIS ASYNCHRONOUSLY!
    do {
	barf(`Fetching entries from ${convoUrl}, page ${convoPage}...`);
	fetcher.open('GET',proxiedConvoUrl+"?page="+convoPage,false); // SYNCHRONOUS request
	fetcher.send();
	if (fetcher.status === 200){
	    var entries = objectify(fetcher.responseXML);
	    convoEntries = mergeTl(entries,convoEntries);
	    (entries.length > 19) ? morePages=true : morePages=false;
	} else {
	    barf("ERROR: HTTP " + fetcher.status);
	    break;
	}
	convoPage++;
    } while (morePages);
    convo.convosChecked.push(convoUrl);
    mergeConvo(convoEntries,convo);
}

function mergeConvo(entries,convo){
    // Pass me the entries from one server's view of the conversation
    // and I'll merge them with the conversation object you've passed.
    
    var newItem;
    var oldItem;
    var mergeCount = 0;
    for (var ix = 0; ix < entries.length ; ix++) {
	newItem = entries[ix];
	oldItem = hasDuplicate(newItem, convo.items,"id");
	// hasDuplicate() returns true if the item is not currently
	// present in convo, or it returns the matching item from
	// convo
	if (oldItem === false){
	    convo.items.push(newItem);
	    mergeCount++;
	} else {
	    // Even if the newItem is a duplicate of one we've
	    // previously collected, it may still be useful.  On some
	    // items in some conversations, the object.inReplyTo
	    // object is missing.  If the version of the item that we
	    // already have in the conversation is missing its
	    // inReplyTo object, see if newItem has it.  If so, copy
	    // it over.
	    if (newItem.replyTo && !oldItem.replyTo){		
		oldItem.replyTo = newItem.replyTo;
	    }
	}	
    }
    barf(`Merged ${mergeCount} items.`);
    doNextPost(convo);
}

function doNextPost(convo){
    var doMeNext = auditConvo(convo);
    if (doMeNext != "none") {
	renderConvo(convo);
	mergeConvoOfPost(doMeNext,convo);
    } else {
	renderConvo(convo);
    }
}
function auditConvo(convo){
    // Do we keep looking for conversations to merge?
    for (var i = 0; i < convo.items.length; i++){
	// Go through the conversation's items, and see if the item's originating
	// Node has been checked yet.

	var itemUrl = convo.items[i].permalink;
	var node = itemUrl.split('/')[2];
	if(!nodeIsChecked(node,convo)){
	    if(itemUrl.includes("/updates/")){
		// Mastodon statuses have an irritating feature in
		// Atom representations where the permalink is of the
		// form:
		//     {server}/users/{username}/updates/{rel_id}
		//
		// where the rel_id is NOT the ID of the post that you
		// need to access it via the API. So, instead, we need
		// to get the objectId out of the post's URI and
		// construct a dummy permalink that
		// postPermalinkToJsonUrl will transform to the API
		// endpoint properly. What a mess.
		var correctId = convo.items[i].id.match(/objectId=(.+):/)[1];
		return itemUrl + "/" + correctId; // HA!
	    } else {
		return itemUrl;
	    }
	}
    }
    return "none";
}
function nodeIsChecked(node,convo){
    for (var i = 0; i < convo.nodesChecked.length; i++){
	if (node == convo.nodesChecked[i]){
	    return true;
	}
    }
    return false;
}
function renderConvo(convo){    
    // Sort items by date, (item.published)
    convo.items.sort(timeCompare);

    for (var i = 0; i < convo.items.length ; i++){
	// If the article is already in ÆGNUS somewhere, just move
	// it into the conversation <div>.
	if(document.getElementById(convo.items[i].id)){
	    article = document.getElementById(convo.items[i].id);
	} else {
	    article = itemToTimelineElement(convo.items[i]);
	}
	convoDiv.appendChild(article);
    }
    
    // then thread them by item.object.inReplyTo.id
    var convoItems = convoDiv.querySelectorAll("article");
    for (var i = 0; i < convo.items.length ; i++){
	var item = convo.items[i];
	if (item.replyTo){
	    var antecedentUrl = item.replyTo;
	    var itemContainer = document.getElementById(item.id);
	    if(document.querySelector('article[data-permalink="'+antecedentUrl+'"]')){
		var antecedentContainer = document.querySelector('article[data-permalink="'+antecedentUrl+'"]').querySelector(".drawer");
		antecedentContainer.appendChild(itemContainer);
	    }
	} else if (item.activityVerb == "share"){
	    var article = document.getElementById(item.id);
	    var drawer = document.getElementById(item.object.id).querySelector(".drawer");
	    if(!drawer.firstElementChild){
	    	drawer.appendChild(article);
	    } else {
	    	drawer.insertBefore(article,drawer.firstElementChild);
	    }
	}
    }
}
function timeCompare(a,b) {
    if (Date.parse(a.published) < Date.parse(b.published))
	return -1;
    else if (Date.parse(a.published) > Date.parse(b.published))
	return 1;
    else 
	return 0;
}

function hasDuplicate(item,items,attr){
    // I compare the passed attribute of an item with those of the passed
    // array's items.  Return the matching item, or 'false' if the
    // item has no duplicate.
    for (var ix = 0; ix < items.length; ix++){
	var oldItem = items[ix];
	if (item[attr] == oldItem[attr]){
	    return items[ix];
	}
    }
    return false;
}
function returnItemsToTimeline(){
    // Since we steal <article>s from the timeline when rendering a 
    // conversation, we want to put them back when we're done. 

    // TODO ??? Integrate the new conversation items back into the
    // timeline instead of deleting them?

    var items = convoDiv.querySelectorAll("article");
    var ids = [];
    var killIds = [];
    for (var ix = 0; ix < items.length; ix++){
	if(hasDuplicate(items.item(ix),tlItems,"id")){
	    // We push the items to an array, because if we move them
	    // now, items inside of other items' drawers will be moved
	    // with them, so we'll have nested articles in the timeline.
	    // And we don't want that.
	    ids.push(items.item(ix));
	}
	else{
	    killIds.push(items.item(ix));
	}
    }
    for (var ix = 0; ix < killIds.length; ix++){
	killIds[ix].parentElement.removeChild(killIds[ix]);
    }
    for (var ix = 0; ix < ids.length; ix++){
	    timelineMain.appendChild(ids[ix]);
    }
    renderTimeline();
}
function barf(text){
    barfDiv.innerHTML+=`<p class="barf">${text}</p>`;
}

    

/* editor.js, part of:
   ÆGNUS:  A GNU social Client  <http://aegn.us/>
   WebExtension version.

   This is free software. It is distributed under the 
   GNU General Public License. <https://www.gnu.org/copyleft/gpl.html>
*/

// -----
// Make the editor, but hide it until summoned.
/* This should be done much better. Editors should be created
   programmatically, so different editor types can be created for
   different post types; and a person should be able to have multiple
   posts/replies being composed at the same time.
*/
var postEditor = document.getElementById("postEditor");
var replyTo = document.getElementById("replyTo");
replyTo.addEventListener("change",checkPermalink);
var resolvedReplyTo = document.getElementById("resolvedReplyTo");
var contentEditor = document.getElementById("contentEditor");
var editorAction = document.getElementById("editorAction");
var converter = document.createElement("div");
var attachmentUrl = document.getElementById("mediaEnclosure");
var tagInput = document.getElementById("silentTags");
var mentionInput = document.getElementById("silentMentions");
var buttonArea = document.createElement("div");
buttonArea.id = "postEditorButtons";
postEditor.appendChild(buttonArea);

// A FUCKING LOT OF BUTTONS
buttonArea.appendChild(editorButton("B","bold"));
buttonArea.appendChild(editorButton("I","italic"));
buttonArea.appendChild(editorButton("U","underline"));
//buttonArea.appendChild(editorButton("S","strike"));
//buttonArea.appendChild(editorButton(" ","superscript"));
//buttonArea.appendChild(editorButton("SUB","subscript"));
buttonArea.appendChild(editorButton("LINK","link"));
//buttonArea.appendChild(editorButton("H1","h1"));
//buttonArea.appendChild(editorButton("H2","h2"));
//buttonArea.appendChild(editorButton("H3","h3"));
buttonArea.appendChild(editorButton("P","p"));
buttonArea.appendChild(editorButton('``',"blockquote"));
buttonArea.appendChild(editorButton("PRE","pre"));
buttonArea.appendChild(editorButton("strip HTML","strip","Remove all HTML formatting from the selected text."));
buttonArea.appendChild(editorButton("parse HTML","htmlize","You may type HTML in the editor, select it, and press this button to have it rendered."));
buttonArea.appendChild(editorButton("show HTML","flurg","Convert the selected text into editable HTML."));

var postButton = editorButton("POST","post");
postButton.style.backgroundColor = "green";
postButton.style.color = "white";
var cancelButton = editorButton("ABORT","cancel");
cancelButton.style.backgroundColor = "red";
cancelButton.style.color = "white";
buttonArea.appendChild(postButton);
buttonArea.appendChild(cancelButton);

// -----------------------------------

function reply(uri){
    itemElement = document.getElementById(uri);
    itemElement.parentElement.insertBefore(postEditor,itemElement);
    postEditor.parentElement.insertBefore(itemElement,postEditor);
    postEditor.style.display = "block";
    postEditor.style.position = "";
    postEditor.style.width = "";
    postEditor.style.height = "";
    postEditor.style.top = "";
    postEditor.style.left = "";
    resolvedReplyTo.textContent = uri;
    editorAction.innerHTML = "&uarr;reply";    
}

function editorButton(value,id,title){
    var newButton = document.createElement("input");
    newButton.type = "button";
    newButton.className = "editorButton";
    if(title) newButton.setAttribute("title",title);
    newButton.value = (value) ? value : "BUTTON";
    newButton.id = (id) ? id : Date.now().toString();
    newButton.addEventListener("click",editorButtonPress);
    return newButton;
}

function editorButtonPress(e){
    switch(e.target.id){
	// Block-level
    case "h1":
	document.execCommand("formatBlock",false,"h1");
	break;
    case "h2":
	document.execCommand("formatBlock",false,"h2");
	break;
    case "h3":
	document.execCommand("formatBlock",false,"h3");
	break;
    case "h4":
	document.execCommand("formatBlock",false,"h4");
	break;
    case "h5":
	document.execCommand("formatBlock",false,"h5");
	break;
    case "h6":
	document.execCommand("formatBlock",false,"h6");
	break;
    case "p":
	document.execCommand("formatBlock",false,"p");
	break;
    case "blockquote":
	document.execCommand("formatBlock",false,"blockquote");
	break;
    case "pre":
	document.execCommand("formatBlock",false,"pre");
	break;
	// Text-level
    case "bold":
	document.execCommand("bold", false);
	break;
    case "italic":
	document.execCommand("italic", false);
	break;
    case "code":
	document.execCommand("formatBlock",false,"code");
	break;
    case "flurg":
	flurg();
	break;
    case "link":
	var linkRef = prompt("URL of hyperlink:");
	document.execCommand("createLink",false,linkRef);
	break;
    case "unlink":
	document.execCommand("unlink",false);
	break;
    case "underline":
	document.execCommand("underline", false);
	break;
    case "strike":
	document.execCommand("strikeThrough", false);
	break;
    case "subscript":
	document.execCommand("subscript", false);
	break;
    case "superscript":
	document.execCommand("superscript", false);
	break;
    case "hilite":
	document.execCommand("HiliteColor", false, "yellow");
	break;
    case "strip":
	document.execCommand("removeFormat", false);
	break;
    case "hrule":
	document.execCommand("insertHorizontalRule",false);
	break;
    case "outdent":
	document.execCommand("outdent",false);
	break;
    case "indent":
	document.execCommand("indent",false);
	break;
    case "htmlize":
	var HTML = window.getSelection().toString();
	document.execCommand("insertHTML", false, HTML);
	break;
    case "post":
	makePost();
	break;
    case "cancel":
	resetEditor();
	break;
    default:
	document.execCommand("insertHTML",false,"<strong>FOO</strong>");
    }
    contentEditor.focus();
}

function resetEditor(){
    editorAction.textContent = "";
    resolvedReplyTo.textContent = "";
    contentEditor.innerHTML = "";
    postEditor.style.display = "none";
    document.body.appendChild(postEditor);
}

function flurg(){
    // Expose HTML of selected text.
    var frag = window.getSelection().getRangeAt(0).copyContents();
    var div = document.createElement("div");
    div.appendChild(frag);
    var newFrag = purifyText(div.innerHTML);
    document.execCommand("insertHTML", false, newFrag);
}

function deletePost(id){
    // Same-Origin Policy doesn't actually let us do this, because the
    // DELETE request needs to be preflighted, and GNU social doesn't
    // send the ...-Allow-Origin: header. *groan*

    // So, open-up the web UI "delete" page in another window/tab.

    var el = document.getElementById(id);
    var localId = el.getAttribute("data-localId");    
    //var apiTarget = getNodeUrl() + "/api/statuses/show/" + localId + ".atom";
    //var xhr = new XMLHttpRequest();
    //xhr.onload = function(el){
    //if (this.status === 200 ||
    //	    this.status === 201){
    //	    document.body.removeChild(el);
    //	    getTimeline();
    //	} else {
    //	    barf("Delete unsuccessful? " + this.status + "br" + purifyText(this.responseText));
    //	}
    //}
    //xhr.open("DELETE",apiTarget + "?" + signRequest("DELETE",apiTarget),true);
    //xhr.setRequestHeader("Accept","*");
    //xhr.setRequestHeader("content-type","text/plain; charset=utf-8");
    //xhr.send();
    var url = getNodeUrl() + "/notice/" + localId + "/delete";
    window.open(url,"_blank");
}

function favorPost(target,perm){
    
    // I couldn't figure out how the fuck to make favoring work via APP, so
    // we do it Twitter-style here.
    
    var postron = new XMLHttpRequest();
    postron.onload = function(){
	if (this.status === 200 || this.status === 201){
	    // ALL GOOD? Refresh the feed to get our like.
	    getTimeline();
	} else {
	    barf("SLIGHT PROBLEM WITH THAT: HTTP " + this.status + "<br>" + 
		 purifyText(this.responseText));
	}
    }.bind(postron);
    outboxURL = getNodeUrl() + "/api/favorites/create.json";
//	localStorage.GNUsocialUser + ".atom";
    postron.open("POST", outboxURL + "?" + signRequest("POST",outboxURL,`id=${target}&`),true);
    postron.setRequestHeader("Accept","*");
    postron.setRequestHeader("content-type","text/plain; charset=utf-8");
    postron.send(post);
}

function makePost(){
    // Make a proper post/note (or post/comment) activity from what's in
    // the editor, and send it to postAtom().

    // TODO: break this thing out into a few functions; make it modular, so that
    // additional post types can be created with makePost(type) or something.

    var replyURI = resolvedReplyTo.textContent;    

    var post = atomPostBody();
    var entry = post.querySelector("entry");
    var objectType = post.createElementNS("http://activitystrea.ms/spec/1.0/","object-type");

    if(replyURI != ""){
	// If this is a reply, make it a comment object
	objectType.appendChild(post.createTextNode("comment"));
    } else {
	// Otherwise, make it a note
	//objectType.appendChild(post.createTextNode("note"));
	objectType.appendChild(post.createTextNode("article"));
    }
    entry.appendChild(objectType);

    var verb = post.createElementNS("http://activitystrea.ms/spec/1.0/","verb");
    verb.appendChild(post.createTextNode("post"));
    entry.appendChild(verb);

    // *** Do something about <atom:title> here ?

    // Handle @mentions, !mentions, and #hashtags ....
    var postText = contentEditor.innerHTML;
    makeMentions(postText,post,entry); // Mentions in the <content> area
    makeMentions(mentionInput.textContent,post,entry); // Mentions in the To: area
    makeGroupMentions(postText,post,entry);
    makeGroupMentions(mentionInput.textContent,post,entry);
    makeTags(postText,post,entry);
    makeTags(tagInput.textContent,post,entry);

    // Create <content> of Atom post
    var content = post.createElementNS("http://www.w3.org/2005/Atom","content");
    content.setAttribute("type","html");
    content.appendChild(post.createTextNode(contentEditor.innerHTML));
    entry.appendChild(content);

    if(replyURI != ""){
	var inReplyTo = post.createElementNS("http://purl.org/syndication/thread/1.0","in-reply-to");
	inReplyTo.setAttribute("ref",replyURI);
	inReplyTo.setAttribute("href",replyURI);
	entry.appendChild(inReplyTo);
    }

    //barf(new XMLSerializer().serializeToString(post));
    postAtom(post);    
}

function makeMentions(text,post,entry){
    // Does not properly handle WebFinger mentions
    var mentions = text.match(/@\w+/g);
    if(mentions){
	for (var m = 0; m < mentions.length; m++){
	    // Strip leading character -- There's probably a smarter way to do the regex
	    // so that this isn't necessary.
	    var mentionName = mentions[m].substr(1);
	    // Try looking in our FOAF first.
	    var person = foaf.find(function(p){
		return p.nickname.toLowerCase() == this;
		},mentionName);
	    if(!person){
		// Try some other lookups...
		continue;
	    }

	    // Make the hyperlink in the post content...
	    // *** THIS NEEDS TO BE DONE PROPERLY ***
	    var astring = `@<a class="h-card mention" href="${person.uri}" title="${person.displayName}">${mentionName}</a>`;
	    contentEditor.innerHTML = contentEditor.innerHTML.replace("@"+mentionName,astring);

	    // Make the mention <link>
	    var mentionElement = post.createElementNS("http://www.w3.org/2005/Atom","link");
	    mentionElement.setAttribute("rel","mentioned");
	    mentionElement.setAttribute("ostatus:object-type","http://activitystrea.ms/schema/1.0/person");
	    mentionElement.setAttribute("href",person.uri);
	    entry.appendChild(mentionElement);
	}
    }
}

function makeGroupMentions(text,post,entry){
    // Does not handle WebFinger-style remote group mentions
    var gmentions = text.match(/!\w+/g);
    if(gmentions){
	for (var g = 0; g < gmentions.length; g++){
	    var groupName = gmentions[g].substr(1);
	    var group = memberships.find(function(p){
		return p.name.toLowerCase() == this;
		},groupName);
	    if(!group){
		// Move on if we don't find the group in our memberships.
		// Is it technically possible to post to groups we're not
		// members of? Possibly, but we're not going to try.
		continue;
	    }
	    // *** THIS PART NEEDS TO BE DONE PROPERLY ***
	    var astring = `!<a class="h-card group" href="${group.id}" title="${group.displayName}">${groupName}</a>`;
	    contentEditor.innerHTML = contentEditor.innerHTML.replace("!"+groupName,astring);

	    // Make the mention <link>
	    var gmentionElement = post.createElementNS("http://www.w3.org/2005/Atom","link");
	    gmentionElement.setAttribute("rel","mentioned");
	    gmentionElement.setAttribute("ostatus:object-type","http://activitystrea.ms/schema/1.0/group");
	    gmentionElement.setAttribute("href",group.id);
	    entry.appendChild(gmentionElement);
	}
    }
}

function makeTags(text,post,entry){
    var tags = text.match(/#\w+/g);
    if(tags){
	for (var t = 0; t < tags.length; t++){
	    var tagName = tags[t].substr(1);
	    
	    // *** THIS NEEDS TO BE DONE PROPERLY ***
	    var gsServer = getNodeUrl();
	    var astring = `#<span class="tag"><a rel="tag" href="${gsServer}/tag/${tagName}">${tagName}</a></span>`;
	    contentEditor.innerHTML = contentEditor.innerHTML.replace("#"+tagName,astring);

	    // Make the mention <link>
	    var categoryElement = post.createElement("category");
	    categoryElement.setAttribute("term",tagName);
	    entry.appendChild(categoryElement);
	}
    }

}

function postAtom(post){
    // Fire our fully-cooked Atom post at the server, and hope it sticks.
    var r = new XMLHttpRequest();
    r.onload = function(){
	if (this.status === 200){
	    convoDiv.innerHTML = "";
	    barf(this.responseText); }
	else if (this.status === 201){
	    // ALL GOOD!
	    resetEditor();
	    barf("Message posted:<br>" + purifyText(this.responseText));
	    getTimeline();
	} else {
	    alert(this.status);
	}
    }.bind(r);
    outboxURL = getNodeUrl() + "/api/statuses/user_timeline/" +
	localStorage.GNUsocialUser + ".atom";
    r.open("POST",outboxURL + "?" + signRequest("POST",outboxURL),true);
    r.setRequestHeader("Accept","*");
    r.setRequestHeader("content-type","text/plain; charset=utf-8");
    r.send(post);
}


function atomPostBody(){
    // Return an empty Atom <entry> for filling with goodness.
    var post = document.implementation.createDocument("http://www.w3.org/2005/Atom","entry");
    post.characterSet = "utf-8";
    //var entry = post.querySelector("entry");
    return post;
}
    
function checkPermalink(){
    // I... don't actually remember why this was/is necessary. 
    resolvedReplyTo.textContent = "checking...";
    var inputUrl = replyTo.value.trim();
    if (inputUrl.substring(0,4) == "http") {
	var jsonUrl = inputUrl.split("/");
	jsonUrl = jsonUrl[0] + "//" + jsonUrl[2] + "/api/statuses/show/" + jsonUrl[jsonUrl.length-1] + ".json";
	var r = new XMLHttpRequest();
	r.onload = function(){
	    if (this.status === 200){
		try { postJson = JSON.parse(this.responseText); }
		catch(err) { console.log(err); }
		if(postJson) {
		    resolvedReplyTo.textContent = postJson.uri; } }
	    else { resolvedReplyTo.textContent = ""; }
	}.bind(r);
	r.open("GET",jsonUrl);
	r.send();
    }
}
	
function compose(type){
    editorAction.textContent = "new note";
    
    postEditor.style.display = "block";
    postEditor.style.position = "fixed";
    postEditor.style.width = "40em";
    postEditor.style.maxHeight = "80%";
    postEditor.style.top = "6em";
    postEditor.style.left = "30%";

    resolvedReplyTo.textContent = "";

    contentEditor.innerHTML = "";
}
